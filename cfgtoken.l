%{
/* Tokenize the configuration file.
 *
 * This is a fairly smart lexer, since we want to give it sufficient
 * intelligence to figure out when to possibly expect a number and do the
 * conversion here.  (That saves us lots of trouble at the yacc stage of
 * things.)  Note that we're not *quite* intelligent enough to be able to
 * handle all-numeric log names, but we do allow quoted strings to coerce
 * matters.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1998, 1999, 2000, 2002, 2011
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/system.h>

#include <cfgparse.h>
#include <internal.h>
#include <util/macros.h>
#include <util/xmalloc.h>

/* Suppress generation of the unused input function by flex. */
#define YY_NO_INPUT

/* cfgdie() is defined in cfgparse.y.  Lexers and parsers are incestuous. */
extern void cfgdie(const char *format, ...);
  
/*
 * The current line number of the file.  We keep track of this ourselves
 * since although lex maintains yylineno, flex doesn't by default, and
 * it's easy for us.  Expose it to the parser as well.
 */
int cfgline = 1;
%}

%start  S_ARGS S_BLOCK S_COMMAND S_SET S_VAL

LETTER  [a-zA-Z]
DIGIT   [0-9]
NAME    {LETTER}({LETTER}|{DIGIT}|_)*
WORD    [^ \"\t\n]+
NUMBER  {DIGIT}+
STRING  \"[^\"]*\"
WSP     [ \t]*

%%

[ \t]+                  /* Ignore whitespace except for newlines. */

<INITIAL,S_BLOCK>#.*\n  cfgline++;

<INITIAL>set            BEGIN S_SET; return SET;

<S_SET>{NAME}           {
                            yylval.string = xstrndup(yytext, yyleng);
                            return VARIABLE;
                        }

<S_SET>=                BEGIN S_VAL; return EQ;

<INITIAL>{NAME}         {
                            BEGIN S_BLOCK;
                            yylval.string = xstrndup(yytext, yyleng);
                            return LABEL;
                        }

<S_BLOCK>\{             return LBRACE;
<S_BLOCK>\}             BEGIN INITIAL; return RBRACE;

<S_BLOCK>analyze{WSP}:  BEGIN S_ARGS; return ANALYZE;
<S_BLOCK>archive{WSP}:  BEGIN S_ARGS; return ARCHIVE;
<S_BLOCK>copy{WSP}:     BEGIN S_ARGS; return COPY;
<S_BLOCK>filter{WSP}:   BEGIN S_ARGS; return FILTER;
<S_BLOCK>log{WSP}:      BEGIN S_ARGS; return LOG;
<S_BLOCK>truncate{WSP}: BEGIN S_ARGS; return TRUNCATE;
<S_BLOCK>restart{WSP}:  BEGIN S_COMMAND; return RESTART;
<S_BLOCK>stop{WSP}:     BEGIN S_COMMAND; return STOP;

<S_COMMAND>run          BEGIN S_ARGS; return RUN;
<S_COMMAND>hup          BEGIN S_ARGS; return HUP;

<S_ARGS>{NUMBER}        {
                            yylval.number = atoi(yytext);
                            return NUMBER;
                        }

<S_ARGS,S_VAL>{WORD}    {
                            yylval.string = xstrndup(yytext, yyleng);
                            return STRING;
                        }

<S_ARGS,S_VAL>{STRING}  {
                            yylval.string = xstrndup(yytext + 1, yyleng - 2);
                            return STRING;
                        }

<S_ARGS>\n              BEGIN S_BLOCK; cfgline++; return EOL;
<S_VAL>\n               BEGIN INITIAL; cfgline++; return EOL;
<INITIAL,S_BLOCK>\n     cfgline++;

\n                      cfgdie("Unexpected end of line");
.                       cfgdie("Syntax error at or near \"%c\"", yytext[0]);

%%

/* Ensure that we exit at the end of the file. */
int yywrap(void) { return 1; }
