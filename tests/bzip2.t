#!/usr/bin/perl
# $Id$
# Test suite for newsyslog with bzip2 compression.

use Sys::Hostname qw(hostname);
$| = 1;
print "1..5\n";

# Run newsyslog on the specified configuration file, returning true if it
# exited with zero status and false otherwise.
sub run_newsyslog {
    my $config = shift;
    my $pid = fork;
    if ($pid == 0) {
        exec ('../newsyslog', '-b', $config) or die "Unable to exec: $!\n";
    } elsif ($pid < 0) {
        die "Unable to fork: $!\n";
    } else {
        waitpid ($pid, 0) or die "waitpid failed: $!\n";
        close HUP;
    }
    return ($? == 0);
}

# Check a directory hierarchy given a list of lists of attributes.  Each list
# is a file or directory; the first element is a file name, the second is the
# type of file (d for directory or f for file), the third is the owner, the
# fourth is the group, the fifth is the mode, and the sixth is the contents.
sub check_tree {
    my $tree = shift;
    for (@$tree) {
        my ($name, $type, $owner, $group, $mode, $contents) = @$_;
        if ($type eq 'f' && !-f $name) {
            print "# $name does not exist or is not a file\n";
            return;
        } elsif ($type eq 'd' && !-d $name) {
            print "# $name does not exist or is not a directory\n";
            return;
        }
        if (defined ($owner) && (stat $name)[4] != $owner) {
            print "# $name owned by ", (stat $name)[4], " not $owner\n";
            return;
        }
        if (defined ($group) && (stat $name)[5] != $group) {
            print "# $name grouped by ", (stat $name)[5], " not $group\n";
            return;
        }
        if (defined ($mode) && ((stat $name)[2] & 07777) != $mode) {
            printf "# $name has mode %o not %o\n", (stat $name)[2], $mode;
            return;
        }
        if (defined ($contents)) {
            my $data;
            if ($name =~ /\.bz2$/) {
                $data = `bzcat '$name'`;
            } else {
                local $/;
                open (DATA, $name) or die "Cannot open $name\n";
                $data = <DATA>;
                close DATA;
            }
            if ($data ne $contents) {
                print "# $name has wrong contents\n";
                print "# === SAW ===\n$data\n# === EXPECTED ===\n$contents\n";
                return;
            }
        }
    }
    return 1;
}

# Set up the directory structure that newsyslog will use.
if (!-f 'basic.t') {
    if (-f 't/basic.t')       { chdir 't'    or die "Can't cd to t: $!\n" }
    elsif (-f '../basic.t')   { chdir '..'   or die "Can't cd to ..: $!\n" }
    elsif (-f '../t/basic.t') { chdir '../t' or die "Can't cd to ../t: $!\n" }
    else { die "Can't find root of test directory.\n" }
}
mkdir ('archive', 0755);
mkdir ('logs', 0755);

# Generate some dummy log files.
my @data = ("one\ntwo\nthree\n", ("\0" x 4000) . ("\1" x 4000),
            "four\nfive\nsix\n");
open (LOG, '> logs/test1') or die "Can't create logs/test1: $!\n";
print LOG $data[0];
close LOG;
open (LOG, '> logs/test2') or die "Can't create logs/test2: $!\n";
print LOG $data[1];
close LOG;
open (LOG, '> logs/test3') or die "Can't create logs/test3: $!\n";
print LOG $data[2];
close LOG;

# Now, fork off newsyslog in the background and go to sleep waiting for it
# to finish (and processing a HUP signal in the meantime).
my ($day, $month, $year) = (localtime time)[3..5];
unless (run_newsyslog ("bzip2.conf")) { print "not " }
print "ok 1\n";
$month++;
$year += 1900;

# Start checking things.  First the various command output from starting and
# stopping things.
undef $/;
my $hostname = hostname;
$hostname =~ s/\..*//;

# Now the structure of the archive area.
my $monthdir = sprintf ("archive/%02d", $month);
my $fulldir = sprintf ("archive/%04d-%02d-%02d", $year, $month, $day);
my $yearbase = sprintf ("%04d.%02d", $year, $day);
my $status = check_tree ([
    [ 'logs/test1', 'f', 0, 0, 0644, '' ],
    [ 'archive/test1 weird%stuff.0.bz2', 'f', undef, undef, undef, $data[0] ],
    [ 'logs/test2', 'f', 0, 0, 0620, '' ],
    [ 'logs/test3', 'f', undef, undef, undef, $data[2] ],
    [ $monthdir, 'd' ],
    [ "$monthdir/$yearbase.test2.0.bz2", 'f', undef, undef, undef, $data[1] ],
    [ "$fulldir/$hostname.test2.bz2", 'f', undef, undef, undef, $data[1] ],
    [ "$fulldir/$hostname.test3.bz2", 'f', undef, undef, undef, $data[2] ]
]);
if (!$status) { print "not " }
print "ok 2\n";

# Run newsyslog on the same configuration file again after deleting the two
# files that won't be rotated.
unlink ("$fulldir/$hostname.test2.bz2", "$fulldir/$hostname.test3.bz2");
unless (run_newsyslog ("bzip2.conf")) { print "not " }
print "ok 3\n";
my $status = check_tree ([
    [ 'logs/test1', 'f', 0, 0, 0644, '' ],
    [ 'archive/test1 weird%stuff.0.bz2', 'f', undef, undef, undef, '' ],
    [ 'archive/test1 weird%stuff.1.bz2', 'f', undef, undef, undef, $data[0] ],
    [ 'logs/test2', 'f', 0, 0, 0620, '' ],
    [ 'logs/test3', 'f', undef, undef, undef, $data[2] ],
    [ $monthdir, 'd' ],
    [ "$monthdir/$yearbase.test2.0.bz2", 'f', undef, undef, undef, '' ],
    [ "$fulldir/$hostname.test2.bz2", 'f', undef, undef, undef, '' ],
    [ "$fulldir/$hostname.test3.bz2", 'f', undef, undef, undef, $data[2] ]
]);
if (!$status) { print "not " }
print "ok 4\n";

# Make sure that the file that should be deleted was.
if (-f "$monthdir/$yearbase.test2.1.bz2") { print "not " }
print "ok 5\n";

# Clean up.
system ('rm', '-rf', 'logs', 'archive');
unlink ('basic.out1', 'basic.out2', 'basic.hup', 'basic.pid');
