#!/usr/bin/perl
# $Id$
# Test suite for truncation.

use Compress::Zlib qw(gzopen);
$| = 1;
print "1..2\n";

# Run newsyslog on the specified configuration file, returning true if it
# exited with zero status and false otherwise.
sub run_newsyslog {
    my $config = shift;
    my $pid = fork;
    if ($pid == 0) {
        exec ('../newsyslog', $config) or die "Unable to exec: $!\n";
    } elsif ($pid < 0) {
        die "Unable to fork: $!\n";
    } else {
        waitpid ($pid, 0) or die "waitpid failed: $!\n";
        close HUP;
    }
    return ($? == 0);
}

# Check a directory hierarchy given a list of lists of attributes.  Each list
# is a file or directory; the first element is a file name, the second is the
# type of file (d for directory or f for file), the third is the owner, the
# fourth is the group, the fifth is the mode, and the sixth is the contents.
sub check_tree {
    my $tree = shift;
    for (@$tree) {
        my ($name, $type, $owner, $group, $mode, $contents) = @$_;
        if ($type eq 'f' && !-f $name) {
            print "# $name does not exist or is not a file\n";
            return;
        } elsif ($type eq 'd' && !-d $name) {
            print "# $name does not exist or is not a directory\n";
            return;
        }
        if (defined ($owner) && (stat $name)[4] != $owner) {
            print "# $name owned by ", (stat $name)[4], " not $owner\n";
            return;
        }
        if (defined ($group) && (stat $name)[5] != $group) {
            print "# $name grouped by ", (stat $name)[5], " not $group\n";
            return;
        }
        if (defined ($mode) && ((stat $name)[2] & 07777) != $mode) {
            printf "# $name has mode %o not %o\n", (stat $name)[2], $mode;
            return;
        }
        if (defined ($contents)) {
            my $data;
            if ($name =~ /\.gz$/) {
                my $gz = gzopen ($name, 'rb')
                    or die "Cannot gzopen $name: $!\n";
                if ($gz->gzread ($data, 1024 * 1024) < 0) {
                    die "Failed to read file $name: ", $gz->gzerror, "\n";
                }
                $gz->gzclose;
            } else {
                local $/;
                open (DATA, $name) or die "Cannot open $name\n";
                $data = <DATA>;
                close DATA;
            }
            if ($data ne $contents) {
                print "# $name has wrong contents\n";
                print "# === SAW ===\n$data\n# === EXPECTED ===\n$contents\n";
                return;
            }
        }
    }
    return 1;
}

# Set up the directory structure that newsyslog will use.
if (!-f 'truncate.t') {
    if (-f 't/truncate.t') {
        chdir 't' or die "Can't cd to t: $!\n";
    } elsif (-f '../truncate.t') {
        chdir '..' or die "Can't cd to ..: $!\n";
    } elsif (-f '../t/truncate.t') {
        chdir '../t' or die "Can't cd to ../t: $!\n";
    } else {
        die "Can't find root of test directory.\n";
    }
}
mkdir ('logs', 0755);

# Generate some dummy log files.
my @data = ("foo\nbar\nbaz foo\nmyfoobar\nmyoobar\n", "baz\nbar\nbaz");
open (LOG, '> logs/test') or die "Can't create logs/test: $!\n";
print LOG $data[0];
close LOG;
open (LOG, '> logs/test1') or die "Can't create logs/test1: $!\n";
print LOG $data[1];
close LOG;

# Run newsyslog.
my $now = time;
unless (run_newsyslog ("truncate.conf")) { print "not " }
print "ok 1\n";

# Check the structure of the archive area.
my $status = check_tree ([
    [ 'logs/test', 'f', 0, 0, 0644, '' ],
    [ 'logs/test1', 'f', 0, 0, 0644, '' ],
    [ 'archive/test1.gz', 'f', undef, undef, undef, $data[1] ]
]);
if (!$status) { print "not " }
print "ok 2\n";

# Clean up.
system ('rm', '-rf', 'logs', 'archive');
