/*
 * Prototypes for internal newsyslog functions.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1997, 1998, 1999, 2000, 2002, 2004, 2005, 2011
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing information.
 */

#ifndef INTERNAL_H
#define INTERNAL_H 1

#include <portable/macros.h>
#include <portable/stdbool.h>

#include <stdio.h>              /* FILE */
#include <sys/types.h>          /* gid_t, mode_t, time_t, uid_t */

/* Whether to use bzip2 instead of gzip for log compression (-b). */
extern bool use_bzip2;

/*
 * The number of minutes by which to skew timestamps when determining dates
 * for expansion of log destination paths (-s).
 */
extern long skew_minutes;

/*
 * A linked list of archive locations and the number of old logs to save (if
 * any) for a log.
 */
struct archive {
    char *path;                 /* Path to the archive location. */
    char *template;             /* Pre-expanded path to the archive. */
    const char *ext;            /* Extension for compressed files. */
    int count;                  /* Number of old logs to save. */
    struct archive *next;
};

/*
 * A linked list of logs to rotate, along with the directories to move them
 * into and the mode and ownership to give the new log.
 */
struct log {
    char *name;                 /* Name of the local logfile. */
    char *path;                 /* Path to the local logfile. */
    char *tmppath;              /* Temporary local file for rotation. */
    struct archive *archive;    /* Locations to archive a copy of it. */
    struct command *analyze;    /* Command to run on the raw log. */
    struct command *filter;     /* Command to filter the log through. */
    bool truncate;              /* Whether to truncate the log. */
    bool rotate;                /* Whether to rotate the log. */
    uid_t owner;                /* Owner of the new logfile. */
    gid_t group;                /* Group of the new logfile. */
    mode_t mode;                /* Permissions for the new logfile. */
    struct log *next;
};

/*
 * A command is an argv vector (a NULL-terminated array of arguments for the
 * command to run), a count of the length of the array, and the amount of
 * allocated memory for it.  We support a linked list of commands for analyze
 * commands.
 */
struct command {
    int argc;                   /* Number of arguments. */
    int size;                   /* Allocated number of arguments. */
    char **argv;                /* NULL-terminated array of arguments. */
    struct command *next;
};

/*
 * A process (either a "run" or a "hup" command, depending on which fields
 * are filled in).  It's a "hup" command if there's a pidfile and a "run"
 * command if arguments is filled in.
 */
struct process {
    char *pidfile;              /* PID file for a hup command. */
    struct command *command;    /* An external command to run. */
};

/*
 * A linked list of classes of logs.  Each class stores the stop and restart
 * commands for the server and a pointer to a list of logs to rotate.
 */
struct class {
    char *name;                 /* Name of the log class. */
    struct process *stop;       /* How to stop the server. */
    struct process *restart;    /* How to restart the server. */
    struct log *logs;           /* List of logs to rotate. */
    struct class *next;
};

BEGIN_DECLS

/* compress.c */

/*
 * Compress a stream into a destination file, optionally with fchmod.  Does
 * not close the stream but leaves it at end of file.
 */
bool compress_stream(FILE *source, const char *destname, mode_t mode);

/* Copy a file from one location to another, compressing it. */
bool compress_file(const char *sourcename, const char *destname);

/* expand.c */

/*
 * Expand a format string that contains % escapes, returning a pointer to
 * the result or NULL in the event of any failure.  The format string looks
 * a bit like a printf() format string, and takes the following special %
 * sequences:
 *
 *     %%   A literal '%' character.
 *     %d   A date, formatted as yyyy-mm-dd, from the time_t passed in.
 *     %D   The two-digit current day of the month.
 *     %M   The two-digit current month of the year (January being 01).
 *     %Y   The four-digit current year.
 *     %m   The machine name, truncated at the first period.
 *     %n   An arbitrary string, from the char * passed in.
 *     %s   An arbitrary string from the variable arguments section.
 *     %t   Seconds since epoch (a Unix timestamp).
 *
 * If there are no %n and %d escapes in the format string, name and date are
 * not used.  The result is newly allocated memory, which the caller is
 * responsible for freeing.
 */
char *expand(const char *format, const char *name, time_t date, ...);

/* file.c */

/* Create a new file with the given name, owner, group, and permissions. */
bool create_file(const char *, uid_t, gid_t, mode_t);

/* Truncate an existing file and ensure the owner, group, and permissions. */
bool truncate_file(const char *, uid_t, gid_t, mode_t);

/* Given a file path, create the directory if it doesn't exist. */
void create_directory(char *path);

/* Copy a file from one location to another. */
bool copy(const char *sourcename, const char *destname);

/* Rename a log to a unique name, returning that name. */
char *rename_unique(const char *filename);

/*
 * Rename files ending in a number and .gz or .bz2 as appropriate for the
 * compression method, only keeping a given number.
 */
bool shuffle_files(const char *filename, const char *extension, int count);

/* logs.c */

/* Read in a configuration file and return a list of log classes. */
struct class *read_config(const char *filename);

/* Perform all of the archiving desired for a given log. */
bool archive_logs(struct log *log);

/* Perform archiving for a log that doesn't need to be rotated. */
void copy_log(const struct log *log);

/* Run a program on a log file. */
void analyze_log(const struct log *log);

/* process.c */

/* Run a program with given arguments, return when it completes. */
extern int run(char *const argv[], const char *input);

/* Send the PID stored in pid_filename a SIGHUP. */
extern int hup(const char *pid_filename);

/* Open a pipe from a process, taking input from the given file. */
extern FILE *open_pipe(char *const argv[], const char *input);

/* Close a pipe, returning success if the program exited successfully. */
extern int close_pipe(FILE *output);

END_DECLS

#endif /* !INTERNAL_H */
