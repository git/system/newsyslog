/*
 * Log copying and archiving.
 *
 * Here are the high level functions to copy or archive logs.  Most of the
 * real work is done by the functions in file.c and process.c, which we call
 * in the functions here.  This is the abstraction layer that sits between
 * main.c and file.c.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1999, 2000, 2002, 2004, 2011
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/system.h>

#include <assert.h>
#include <ctype.h>
#include <dirent.h>
#include <sys/stat.h>
#include <time.h>

#include <internal.h>
#include <util/messages.h>
#include <util/xmalloc.h>


/* Linked list of log files to archive, sorted numerically. */
struct logfile {
    char *file;                 /* Name of log file. */
    time_t date;                /* Timestamp of file (from file name). */
    struct logfile *next;
};


/*
 * Scan through the given directory, finding all files starting with the given
 * base filename followed by at least nine digits.  Insert these into a linked
 * list, sorted by the number in the file extension, and return that list.
 */
static struct logfile *
find_logs(const char *dir, const char *base)
{
    DIR *d;
    const char *date, *p;
    unsigned long timestamp;
    struct dirent *file;
    bool okay;
    size_t offset;
    struct logfile *log, *current;
    struct logfile *logs = NULL;

    d = opendir(dir);
    if (d == NULL) {
        syswarn("Unable to open directory %s", dir);
        return NULL;
    }
    offset = strlen(base);
    while ((file = readdir(d)) != NULL) {
        if (strlen(file->d_name) < offset + 10)
            continue;
        if (strncmp(file->d_name, base, offset))
            continue;
        if (file->d_name[offset] != '.')
            continue;
        okay = true;
        for (p = file->d_name + offset + 1; *p; p++)
            if (!isdigit((unsigned char) *p))
                okay = false;
        if (!okay || (unsigned) (p - file->d_name) < offset + 10)
            continue;

        /* We found an old log.  Add it to the list. */
        log = xmalloc(sizeof(struct logfile));
        xasprintf(&log->file, "%s/%s", dir, file->d_name);
        log->date = 0;
        date = strrchr(log->file, '.');
        assert(date != NULL);
        if (sscanf(date + 1, "%lu", &timestamp) != 1)
            warn("Parsing of time from %s failed", date);
        else
            log->date = timestamp;
        if (!logs || difftime(logs->date, log->date) > 0) {
            log->next = logs;
            logs = log;
        } else {
            for (current = logs; current->next != NULL;
                 current = current->next)
                if (difftime(current->next->date, log->date) > 0)
                    break;
            log->next = current->next;
            current->next = log;
        }
    }
    closedir(d);
    return logs;
}


/*
 * Run an external filter program on a log and save the output in compressed
 * form to the given file.  Returns true on success and false on failure.
 */
static bool
filter_log(const char *sourcename, const char *destname, char *const argv[])
{
    FILE *source;
    bool okay;
    struct stat stbuf;
    mode_t mode = 0;

    if (stat(sourcename, &stbuf) == 0)
        mode = stbuf.st_mode;
    source = open_pipe(argv, sourcename);
    if (source == NULL)
        return false;
    okay = compress_stream(source, destname, mode);
    if (!close_pipe(source))
        okay = false;
    return okay;
}


/*
 * Take in a log struct for a log that we're just copying.  Since we never
 * rename those logs, we can't tell if there was a previous failed attempt, so
 * we don't bother looking for old failures.
 */
void
copy_log(const struct log *log)
{
    char *template, *path;
    const struct archive *current;
    time_t now;

    for (current = log->archive; current != NULL; current = current->next) {
        now = time(NULL) - (skew_minutes * 60);
        template = expand(current->template, log->name, now);
        xasprintf(&path, "%s.%s", template, current->ext);
        free(template);
        if (access(path, F_OK) == 0 && unlink(path) != 0)
            syswarn("Unable to remove %s", path);
        create_directory(path);
        if (log->filter != NULL)
            filter_log(log->path, path, log->filter->argv);
        else
            compress_file(log->path, path);
        free(path);
    }
}


/*
 * We take in a log struct.  From this, we determine the base name of other
 * temporary log files and scan the directory the temporary log file is
 * located in for other temporary files (old failed log rotations).  We then
 * step through the archive instructions for this log and archive all of the
 * logs found (in order from oldest to current).
 */
bool
archive_logs(struct log *log)
{
    char *p, *path, *template;
    const char *base, *directory;
    bool okay, old;
    struct archive *current;
    struct logfile *files, *file;
    time_t date;

    /* Find the directory all of our logs will be in. */
    p = strrchr(log->path, '/');
    if (p == NULL)
        directory = ".";
    else if (p == log->path)
        directory = "/";
    else {
        directory = log->path;
        *p = '\0';
    }
    base = p + 1;

    /*
     * Scan the directory for failed log rotations, and then archive all of
     * the logs, today's and old failures.
     */
    files = find_logs(directory, base);
    if (files == NULL) {
        warn("Unable to find %s", log->tmppath);
        return false;
    }
    if (p != NULL && *p == '\0')
        *p = '/';
    for (file = files; file != NULL; file = file->next) {
        okay = true;
        old = false;
        for (current = log->archive; current != NULL;
             current = current->next) {
            date = file->date;
            if (date == 0)
                date = time(NULL) - (skew_minutes * 60);
            template = expand(current->template, log->name, date);
            if (current->count > 0)
                okay &= shuffle_files(template, current->ext, current->count);
            create_directory(template);
            xasprintf(&path, "%s%s.%s", template,
                      (current->count > 0) ? ".0" : "", current->ext);
            free(template);
            if (current->count < 0 && access(path, F_OK) == 0)
                if (unlink(path) != 0) {
                    syswarn("Unable to remove %s", path);
                    okay = false;
                }
            if (log->filter) {
                if (!filter_log(file->file, path, log->filter->argv))
                    okay = false;
            } else {
                if (!compress_file(file->file, path))
                    okay = false;
            }
            if (!okay)
                break;
            if (!old && strcmp(log->tmppath, file->file)) {
                old = true;
                warn("Archived old log %s", file->file);
            }
            if (old)
                warn("   as %s", path);
            free(path);
        }
        if (okay && unlink(file->file) != 0)
            syswarn("Unable to delete temporary file %s", file->file);
    }

    /* Blow away our allocated structures and return success. */
    while (files != NULL) {
        file = files;
        files = files->next;
        free(file->file);
        free(file);
    }
    return true;
}


/*
 * Run an external log analysis program on a log.  This program is presumed to
 * handle its own output and error messages, and failure of this program
 * doesn't cause failure of the log rotation.
 */
void
analyze_log(const struct log *log)
{
    const char *path;

    path = (log->tmppath != NULL) ? log->tmppath : log->path;
    run(log->analyze->argv, path);
}
