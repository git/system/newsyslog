/*
 * Main entry point for log file rotation and periodic maintenance.
 *
 * This program is designed to rotate, truncate, or otherwise deal with system
 * logs on a regular basis, including correctly stopping and restarting
 * daemons so that the logs can be moved and compressed safely.  It takes a
 * configuration file specifying which logs to rotate and what to do with
 * them.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1997, 1998, 1999, 2000, 2002, 2004, 2011
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/system.h>

#include <errno.h>
#include <sys/stat.h>

#include <internal.h>
#include <util/messages.h>

/* Global variable saying whether to use bzip2 for log compression. */
bool use_bzip2 = false;

/* Global variable saying how many minutes by which to skew times. */
long skew_minutes = 0;

/* The usage message. */
const char usage_message[] = "\
Usage: newsyslog [-bchv] [-s <minutes>] [<config-file>]\n\
  -b            Use bzip2 to compress log files instead of gzip\n\
  -c            Just syntax-check the configuration file, don't do anything\n\
  -h            Prints this message\n\
  -s <minutes>  Skew all timestamps by subtracting <minutes>\n\
  -v            Prints the version of newsyslog\n\
\n\
newsyslog rotates, archives, copies, and/or truncates log files as\n\
instructed in a configuration file, including running commands before\n\
and after if desired.  If not given on the command line, the default\n\
configuration file is %s.";


/*
 * Print out the usage message and then exit with the status given as the
 * only argument.  If status is zero, the message is printed to standard
 * output; otherwise, it is sent to standard error.
 */
static void
usage(int status)
{
    fprintf((status == 0) ? stdout : stderr, usage_message, CONFIG_FILE);
    exit(status);
}


/*
 * From a process struct, either send HUP to a process or run a command.
 */
static void
process(struct process *action)
{
    if (action->pidfile)
        hup(action->pidfile);
    else if (action->command)
        run(action->command->argv, NULL);
}


/*
 * First, read in the configuration file, either the hard-coded value or the
 * first command-line argument.  This returns a linked list of log classes.
 * Next, step through each log class, calling the stop command if necessary,
 * rotate each of the logs in that class, call the restart command, and for
 * each log copy it to its archive locations while compressing.
 *
 * See internal.h for our data structure here.  We don't bother freeing memory
 * as we go; there's no point, since we build up this structure, execute it,
 * and then exit.
 */
int
main(int argc, char **argv)
{
    struct class *class, *current;
    struct log *log;
    int opt;
    const char *conffile;
    char *end;
    bool check_only = false;
    struct stat st;

    /* Look for command-line options. */
    while ((opt = getopt(argc, argv, "bchs:v")) != EOF)
        switch (opt) {
        case 'b':
            use_bzip2 = true;
            break;
        case 'c':
            check_only = true;
            break;
        case 'h':
            usage(0);
            break;
        case 's':
            errno = 0;
            skew_minutes = strtol(optarg, &end, 10);
            if (skew_minutes < 0 || errno != 0 || *end != '\0')
                die("invalid argument to -s: %s", optarg);
            break;
        case 'v':
            printf("%s\n", PACKAGE_STRING);
            exit(0);
        default:
            usage(1);
            break;
        }
    argc -= optind;
    argv += optind;
    if (argc > 1)
        usage(1);

    /* If we have a command-line argument, assume that's the config file. */
    conffile = (argc > 0) ? argv[0] : CONFIG_FILE;
    class = read_config(conffile);
    if (check_only) {
        printf("Syntax of %s is valid\n", conffile);
        exit(0);
    }

    for (current = class; current != NULL; current = current->next) {
        if (current->stop != NULL)
            process(current->stop);

        /*
         * Rotate all the logs that have directives.  Skip anything that isn't
         * a regular file; usually this means that a directory was mistakenly
         * specified instead of the log file, and we can't recreate
         * non-regular files properly anyway.
         */
        for (log = current->logs; log != NULL; log = log->next) {
            if (stat(log->path, &st) < 0)
                syswarn("Cannot stat %s", log->path);
            else if (!S_ISREG(st.st_mode)) {
                warn("%s is not a regular file", log->path);
                continue;
            }
            if (log->rotate) {
                log->tmppath = rename_unique(log->path);
                if (log->tmppath)
                    create_file(log->path, log->owner, log->group, log->mode);
            }
        }

        if (current->restart != NULL)
            process(current->restart);

        for (log = current->logs; log != NULL; log = log->next) {
            if (log->tmppath == NULL && log->rotate) {
                if (log->archive->template != NULL)
                    warn("Skipping archiving of %s", log->name);
            } else {
                if (log->analyze != NULL)
                    analyze_log(log);
                if (log->rotate)
                    archive_logs(log);
                else if (log->archive != NULL)
                    copy_log(log);
                if (log->truncate)
                    truncate_file(log->path, log->owner, log->group,
                                  log->mode);
            }
        }
    }

    /* Success.  Exit. */
    exit(0);
}
