#!/usr/bin/perl
# Test suite for newsyslog with time skew configured.

use Compress::Zlib qw(gzopen);
use Sys::Hostname qw(hostname);
$| = 1;
print "1..6\n";

# Run newsyslog with the specified options and configuration file, returning
# true if it exited with zero status and false otherwise.
sub run_newsyslog {
    my $pid = open (NEWSYSLOG, '-|');
    my $output;
    if ($pid == 0) {
        open (STDERR, '>&STDOUT') or die "Can't dup stdout: $!\n";
        exec ('../newsyslog', @_) or die "Unable to exec: $!\n";
    } elsif ($pid < 0) {
        die "Unable to fork: $!\n";
    }
    local $/;
    $output = <NEWSYSLOG>;
    close NEWSYSLOG;
    return ($?, $output);
}

# Check a directory hierarchy given a list of lists of attributes.  Each list
# is a file or directory; the first element is a file name, the second is the
# type of file (d for directory or f for file), the third is the owner, the
# fourth is the group, the fifth is the mode, and the sixth is the contents.
sub check_tree {
    my $tree = shift;
    for (@$tree) {
        my ($name, $type, $owner, $group, $mode, $contents) = @$_;
        if ($type eq 'f' && !-f $name) {
            print "# $name does not exist or is not a file\n";
            return;
        } elsif ($type eq 'd' && !-d $name) {
            print "# $name does not exist or is not a directory\n";
            return;
        }
        if (defined ($owner) && (stat $name)[4] != $owner) {
            print "# $name owned by ", (stat $name)[4], " not $owner\n";
            return;
        }
        if (defined ($group) && (stat $name)[5] != $group) {
            print "# $name grouped by ", (stat $name)[5], " not $group\n";
            return;
        }
        if (defined ($mode) && ((stat $name)[2] & 07777) != $mode) {
            printf "# $name has mode %o not %o\n", (stat $name)[2], $mode;
            return;
        }
        if (defined ($contents)) {
            my $data;
            if ($name =~ /\.gz$/) {
                my $gz = gzopen ($name, 'rb')
                    or die "Cannot gzopen $name: $!\n";
                if ($gz->gzread ($data, 1024 * 1024) < 0) {
                    die "Failed to read file $name: ", $gz->gzerror, "\n";
                }
                $gz->gzclose;
            } else {
                local $/;
                open (DATA, $name) or die "Cannot open $name\n";
                $data = <DATA>;
                close DATA;
            }
            if ($data ne $contents) {
                print "# $name has wrong contents\n";
                print "# === SAW ===\n$data\n# === EXPECTED ===\n$contents\n";
                return;
            }
        }
    }
    return 1;
}

# Set up the directory structure that newsyslog will use.
if (!-f 'basic.t') {
    if (-f 't/basic.t')       { chdir 't'    or die "Can't cd to t: $!\n" }
    elsif (-f '../basic.t')   { chdir '..'   or die "Can't cd to ..: $!\n" }
    elsif (-f '../t/basic.t') { chdir '../t' or die "Can't cd to ../t: $!\n" }
    else { die "Can't find root of test directory.\n" }
}
mkdir ('archive', 0755);
mkdir ('logs', 0755);

# Generate some dummy log files.
my @data = ("one\ntwo\nthree\n", ("\0" x 4000) . ("\1" x 4000));
open (LOG, '> logs/test1') or die "Can't create logs/test1: $!\n";
print LOG $data[0];
close LOG;
open (LOG, '> logs/test2') or die "Can't create logs/test2: $!\n";
print LOG $data[1];
close LOG;

# Now, fork off newsyslog in the background and go to sleep waiting for it
# to finish (and processing a HUP signal in the meantime).
my ($day, $month, $year) = (localtime (time - 24 * 60 * 60))[3..5];
my ($status, $output) = run_newsyslog ('-s', 24 * 60, 'skew.conf');
unless ($status == 0) { print "not " }
print "ok 1\n";
unless ($output eq '') { print "not " }
print "ok 2\n";
$month++;
$year += 1900;

# Start checking things.  First the various command output from starting and
# stopping things.
undef $/;
my $hostname = hostname;
$hostname =~ s/\..*//;

# Now the structure of the archive area.
my $monthdir = sprintf ("archive/%02d", $month);
my $fulldir = sprintf ("archive/%04d-%02d-%02d", $year, $month, $day);
my $yearbase = sprintf ("%04d.%02d", $year, $day);
my $status = check_tree ([
    [ 'logs/test1', 'f', 0, 0, 0620, '' ],
    [ 'logs/test2', 'f', undef, undef, undef, $data[1] ],
    [ $monthdir, 'd' ],
    [ "$monthdir/$yearbase.test1.0.gz", 'f', undef, undef, undef, $data[0] ],
    [ "$fulldir/$hostname.test1.gz", 'f', undef, undef, undef, $data[0] ],
    [ "$fulldir/$hostname.test2.gz", 'f', undef, undef, undef, $data[1] ]
]);
if (!$status) { print "not " }
print "ok 3\n";

# Run newsyslog on the same configuration file again and test that the
# timestamps of temporary rotation files are correct.
my $now = time - (24 * 60 * 60);
($status, $output) = run_newsyslog ('-s', 24 * 60, 'skew.conf');
unless ($status == 0) { print "not " }
print "ok 4\n";
unless ($output =~ m%^Cannot create \Q$fulldir/$hostname.test1.gz: %) {
    print "not ";
}
print "ok 5\n";
my $status = check_tree ([
    [ 'logs/test1', 'f', 0, 0, 0620, '' ],
    [ "logs/test1.$now", 'f', 0, 0, 0620, '' ],
    [ 'logs/test2', 'f', undef, undef, undef, $data[1] ],
    [ $monthdir, 'd' ],
    [ "$monthdir/$yearbase.test1.0.gz", 'f', undef, undef, undef, '' ],
    [ "$monthdir/$yearbase.test1.1.gz", 'f', undef, undef, undef, $data[0] ],
    [ "$fulldir/$hostname.test1.gz", 'f', undef, undef, undef, $data[0] ],
    [ "$fulldir/$hostname.test2.gz", 'f', undef, undef, undef, $data[1] ]
]);
if (!$status) { print "not " }
print "ok 6\n";

# Clean up.
system ('rm', '-rf', 'logs', 'archive');
unlink ('basic.out1', 'basic.out2', 'basic.hup', 'basic.pid');
