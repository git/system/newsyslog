/*
 * Functions to expand % escapes in strings.
 *
 * A collection of functions used to generate filenames and a few other things
 * from strings containing % escapes of various types as templates.  We need
 * this collection of functions to support specifying pretty much arbitrary
 * naming schemes for the rotated logs.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1997, 1999, 2000, 2011
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/system.h>

#include <sys/param.h>
#include <time.h>

#include <internal.h>
#include <util/messages.h>
#include <util/xmalloc.h>

/* Solaris doesn't have MAXHOSTNAMELEN. */
#ifndef MAXHOSTNAMELEN
# define MAXHOSTNAMELEN 64
#endif


/*
 * Construct the name of the local machine, truncated after the first period,
 * and return a pointer to it.  Memory is allocated to store the machine name
 * and the caller is responsible for freeing that memory.  Returns NULL on any
 * sort of error.
 */
static const char *
build_machine(void)
{
    char *period;
    char hostname[MAXHOSTNAMELEN];

    /* gethostname() isn't guaranteed to null-terminate. */
    if (gethostname(hostname, MAXHOSTNAMELEN) != 0) {
        syswarn("gethostname failed");
        return NULL;
    }
    hostname[MAXHOSTNAMELEN - 1] = '\0';
    period = strchr(hostname, '.');
    if (period != NULL)
        *period = '\0';
    return xstrdup(hostname);
}


/*
 * This is the function that other files should call.  It takes a format
 * string and some additional information, expands % escapes in the format
 * string, and returns a pointer to the resulting string or NULL in the event
 * of any failure.  Space for the generated string is malloc()ed and the
 * calling function is responsible for freeing it.
 *
 * The format string looks a bit like a printf() format string, and takes the
 * following special % sequences:
 *
 *     %%   A literal '%' character.
 *     %d   A date, formatted as yyyy-mm-dd, from the time_t passed in.
 *     %D   The two-digit current day of the month.
 *     %M   The two-digit current month of the year (January being 01).
 *     %Y   The four-digit current year.
 *     %m   The machine name, truncated at the first period.
 *     %n   An arbitrary string, from the char * passed in.
 *     %s   An arbitrary string from the variable arguments section.
 *     %t   Seconds since epoch (a Unix timestamp)
 *
 * The passed date and name arguments aren't used unless the corresponding
 * escapes are present in the format string.  Some care is taken to handle
 * arbitrary length in each of these fields, although hostnames are truncated
 * at some maximum size (generally at about 63 characters).  Be aware that the
 * total length of any path on a system may be limited by the kernel to
 * MAXPATHLEN.  Some systems also limit each component to MAXNAMELEN.  This is
 * *not* checked -- caveat caller.
 *
 * Note that the way we call this is a little strange, due to how it has to be
 * used in other parts of the program.  Some calls won't know how many
 * instances of the % escapes there will be; some calls will be making their
 * own format.  %d and %n are derived from regularly passed parameters that we
 * can guarantee will always exist.  %s escapes are "internal," config files
 * shouldn't be permitted to use them, and they use the variable number of
 * arbitrary strings in the variable section of the arguments list.
 *
 * The result is a lot uglier than I'd like.  Ah well.
 *
 * Note that there is no way of checking to make sure that there are the
 * "right" number of variable arguments.  If you give this function more %s
 * escapes than you give it strings, nasty things happen.  You've been warned.
 */
char *
expand(const char *format, const char *name, time_t date, ...)
{
    va_list args;
    char *result, *dest;
    const char *src, *string;
    int length = 0;

    /* Since we may use it many times, store the machine name statically. */
    static const char *machine = NULL;

    /* Sanity check. */
    if (format == NULL)
        return NULL;

    /* If the date we were given was 0, use the current time. */
    if (date == 0) {
        date = time(NULL);
        if (date == (time_t) -1) {
            syswarn("Unable to obtain current time");
            return NULL;
        }
    }

    /* First, we have to determine how long the result will be. */
    va_start(args, date);
    for (src = format; *src != '\0'; src++) {
        if (*src != '%') {
            length++;
        } else {
            switch (*(++src)) {
            case '%':
                length++;
                break;

            case 'd':
                /* Dates are always yyyy-mm-dd (ten characters). */
                length += 10;
                break;

            case 'D':
            case 'M':
                length += 2;
                break;

            case 'Y':
                length += 4;
                break;

            case 't':
                /* Good for another 2,000 years. */
                length += 11;
                break;

            case 'm':
                /* If we don't already know the machine name, find out. */
                if (machine == NULL)
                    machine = build_machine();
                if (machine == NULL)
                    return NULL;
                length += strlen(machine);
                break;

            case 'n':
                if (name == NULL) {
                    warn("No value for %%n given in expand");
                    return NULL;
                }
                length += strlen(name);
                break;

            case 's':
                /* If no corresponding string, segfault. */
                string = va_arg(args, const char *);
                length += strlen(string);
                break;

            default:
                warn("Invalid format in expand: %s", format);
                return NULL;
            }
        }
    }
    va_end(args);

    /* Allow for the trailing null. */
    length++;

    /* Now allocate memory for the result and do the real work. */
    result = xmalloc(length);
    va_start(args, date);
    for (src = format, dest = result; *src; src++) {
        if (*src != '%') {
            *dest++ = *src;
        } else {
            switch (*(++src)) {
            case '%':
                *dest++ = '%';
                break;

            case 'd':
                if (!strftime(dest, 11, "%Y-%m-%d", localtime(&date))) {
                    free(result);
                    return NULL;
                }
                dest += 10;
                break;

            case 'D':
                if (!strftime(dest, 3, "%d", localtime(&date))) {
                    free(result);
                    return NULL;
                }
                dest += 2;
                break;

            case 'M':
                if (!strftime(dest, 3, "%m", localtime(&date))) {
                    free(result);
                    return NULL;
                }
                dest += 2;
                break;

            case 'Y':
                if (!strftime(dest, 5, "%Y", localtime(&date))) {
                    free(result);
                    return NULL;
                }
                dest += 4;
                break;

            case 'm':
                /* Guaranteed that machine is non-NULL (see above). */
                memcpy(dest, machine, strlen(machine));
                dest += strlen(machine);
                break;

            case 'n':
                /* Guaranteed that name is non-NULL (see above). */
                memcpy(dest, name, strlen(name));
                dest += strlen(name);
                break;

            case 's':
                string = va_arg(args, const char *);
                memcpy(dest, string, strlen(string));
                dest += strlen(string);
                break;

            case 't':
                snprintf(dest, 12, "%lu", (unsigned long) date);
                dest += strlen(dest);
                break;

            default:
                /* Impossible. */
                warn("Invalid format in expand: %s", format);
                free(result);
                return NULL;
            }
        }
    }
    va_end (args);

    /* Null-terminate the result string and return it. */
    *dest = '\0';
    return result;
}
