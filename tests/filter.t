#!/usr/bin/perl
# $Id$
# Test suite for newsyslog filtering features.

use Compress::Zlib qw(gzopen);
$| = 1;
print "1..3\n";

# Run newsyslog on the specified configuration file, returning true if it
# exited with zero status and false otherwise.
sub run_newsyslog {
    my $config = shift;
    my $pid = fork;
    if ($pid == 0) {
        exec ('../newsyslog', $config) or die "Unable to exec: $!\n";
    } elsif ($pid < 0) {
        die "Unable to fork: $!\n";
    } else {
        waitpid ($pid, 0) or die "waitpid failed: $!\n";
        close HUP;
    }
    return ($? == 0);
}

# Check a directory hierarchy given a list of lists of attributes.  Each list
# is a file or directory; the first element is a file name, the second is the
# type of file (d for directory or f for file), the third is the owner, the
# fourth is the group, the fifth is the mode, and the sixth is the contents.
sub check_tree {
    my $tree = shift;
    for (@$tree) {
        my ($name, $type, $owner, $group, $mode, $contents) = @$_;
        if ($type eq 'f' && !-f $name) {
            print "# $name does not exist or is not a file\n";
            return;
        } elsif ($type eq 'd' && !-d $name) {
            print "# $name does not exist or is not a directory\n";
            return;
        }
        if (defined ($owner) && (stat $name)[4] != $owner) {
            print "# $name owned by ", (stat $name)[4], " not $owner\n";
            return;
        }
        if (defined ($group) && (stat $name)[5] != $group) {
            print "# $name grouped by ", (stat $name)[5], " not $group\n";
            return;
        }
        if (defined ($mode) && ((stat $name)[2] & 07777) != $mode) {
            printf "# $name has mode %o not %o\n", (stat $name)[2], $mode;
            return;
        }
        if (defined ($contents)) {
            my $data;
            if ($name =~ /\.gz$/) {
                my $gz = gzopen ($name, 'rb')
                    or die "Cannot gzopen $name: $!\n";
                if ($gz->gzread ($data, 1024 * 1024) < 0) {
                    die "Failed to read file $name: ", $gz->gzerror, "\n";
                }
                $gz->gzclose;
            } else {
                local $/;
                open (DATA, $name) or die "Cannot open $name\n";
                $data = <DATA>;
                close DATA;
            }
            if ($data ne $contents) {
                print "# $name has wrong contents\n";
                print "# === SAW ===\n$data\n# === EXPECTED ===\n$contents\n";
                return;
            }
        }
    }
    return 1;
}

# Set up the directory structure that newsyslog will use.
if (!-f 'filter.t') {
    if (-f 't/filter.t')       { chdir 't'    or die "Can't cd to t: $!\n" }
    elsif (-f '../filter.t')   { chdir '..'   or die "Can't cd to ..: $!\n" }
    elsif (-f '../t/filter.t') { chdir '../t' or die "Can't cd to ../t: $!\n" }
    else { die "Can't find root of test directory.\n" }
}
mkdir ('logs', 0755);

# Generate some dummy log files.
my $data = "foo\nbar\nbaz foo\nmyfoobar\nmyoobar\n";
my $filter = "bar\nmyoobar\n";
my $reverse = "foo\nbaz foo\nmyfoobar\n";
open (LOG, '> logs/test') or die "Can't create logs/test: $!\n";
print LOG $data;
close LOG;
open (LOG, '> logs/test2') or die "Can't create logs/test2: $!\n";
print LOG $data;
close LOG;

# Run newsyslog.
unless (run_newsyslog ("filter.conf")) { print "not " }
print "ok 1\n";

# Check the structure of the archive area.
my $status = check_tree ([
    [ 'logs/test', 'f', 0, 0, 0644, '' ],
    [ 'logs/saved.gz', 'f', undef, undef, undef, $data ],
    [ 'logs/saved2.gz', 'f', undef, undef, undef, $filter ],
    [ 'logs/saved3.gz', 'f', undef, undef, undef, $reverse ]
]);
if (!$status) { print "not " }
print "ok 2\n";

# Check the contents of the filtering results.
my $status = check_tree ([
    [ 'logs/filter1', 'f', undef, undef, undef, $filter ],
    [ 'logs/filter2foo3', 'f', undef, undef, undef, $filter ]
]);
if (!$status) { print "not " }
print "ok 3\n";

# Clean up.
unlink ('logs/test', 'logs/test2', 'logs/saved.gz', 'logs/saved2.gz',
        'logs/saved3.gz', 'logs/filter1', 'logs/filter2foo3');
rmdir 'logs';
