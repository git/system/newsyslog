%{
/*
 * Parse the configuration file.
 *
 * Our end product is a linked list of log classes, which the rest of
 * newsyslog will use as data to do the actual work (see logs.h for the
 * definitions of all of the relevant structs).  The input stream comes from
 * the lexer.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1998, 1999, 2000, 2002, 2011
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/system.h>

#include <grp.h>
#include <pwd.h>

#include <internal.h>
#include <util/messages.h>
#include <util/xmalloc.h>

/* We want to have verbose parse error messages. */
#define YYERROR_VERBOSE 1

/* These two functions need to be externally visible. */
void cfgdie(const char *format, ...);
void yyerror(const char *error);

/* This comes from cfgtoken.l and lex. */ 
int yylex(void); 

/* Stuff for internal use only. */
static struct archive *archive(char *template, int count);
static char *cfgexpand(char *string, const char *name);
static char *lookup(char *path);
static void set(char *name, const char *content);
static struct log *merge(struct log *logs, struct log *actions);

/* The FILE that our lexer will read from. */
extern FILE *yyin;

/* The current line of our configuration file (maintained by the lexer). */
extern int cfgline; 

/* Struct to store variable content mappings from set commands. */
struct variable {
    char *name;
    const char *content;
    struct variable *next;
};

/* Stores a linked list of all variables that have been set. */
static struct variable *variables = NULL;
  
/* The final result of our parsing, a linked list of classes. */
static struct class *class;
%}

%start file

%union {
    char *string;
    int number;
    struct command *command;
    struct process *process;
    struct log *log;
    struct class *class;
}

%token ANALYZE ARCHIVE COPY EOL FILTER HUP LOG RESTART RUN SET STOP TRUNCATE
%token EQ "="
%token LBRACE "{"
%token RBRACE "}"
%token <string> LABEL STRING VALUE VARIABLE
%token <number> NUMBER

%type <command> arguments
%type <process> stop restart process hup run
%type <number>  uid gid
%type <log>     logs log actions action analyze archive copy filter truncate
%type <class>   class block

%%

/*
 * Here's a rough overview of the structure of our parser, and the structure
 * of our configuration file in general.  yacc syntax elements are noted in
 * parens near where they're discussed; for example, the top level syntax
 * element is a file (=> file).
 *
 * The configuration file is block structured.  It consists of a number of
 * blocks of related log rotations (=> block); each one of those blocks
 * becomes a struct class.  These blocks are named, but the name is not
 * currently used for anything particularly interesting.  Outside of the
 * blocks, variables can also be set; the syntax to set a variable is:
 *
 *      set <variable> = <value>
 *
 * (=> set).  A log class consists of an optional stop directive (=> stop),
 * an optional restart directive (=> restart), a series of declarations of
 * logs (=> logs), and a series of actions to perform on logs (=> actions),
 * in that order.  The order is required.
 *
 * The data structure we're generating is a linked list of class structs.
 * Each one contains a stop process, a restart process, and a linked list of
 * logs.  Each log contains the information defined about it in the log line
 * (=> log) along with a linked list of actions (archive structs) from the
 * archive (=> archive), copy (=> copy), truncate (=> truncate), and analyze
 * (=> analyze) directives related to it.  The same struct is used to
 * represent both archive and copy; a copy is just a struct archive with -1 as
 * the count of old logs to save.
 *
 * Note that although the list of archive, copy, truncate, and analyze actions
 * are a property of each individual log, our configuration file syntax
 * doesn't reflect that.  To make this really easy to parse, each log should
 * be another block that contains all of its archive and copy actions.  That
 * was more nesting than I wanted, though, so instead one declares all logs up
 * front and then lists all the archive, copy, and analyze directives for all
 * of the logs.
 *
 * Because of this, the parser doesn't have easy access to the linked list
 * of logs built up from all the log lines when it's parsing the archive and
 * copy actions.  Instead, it generates two linked lists of logs; the first
 * contains all of the regular log information (location, owner, group,
 * mode, etc.), and the second contains just the linked lists of archive
 * structs.  The shared piece of information is the log name.  Then, after
 * the parsing of the block is finished, the parser calls merge() to merge
 * those two linked lists into a single linked list of log structs, matching
 * up the individual records (and checking for duplicates, actions that
 * refer to undeclared logs, and so forth).
 *
 * The parser is not white-space sensitive (in fact, white space is taken
 * care of by the lexer) except for newlines; all instructions within a
 * block are terminated by newlines.
 *
 * Like most yacc grammars, this is much easier to read from the bottom up
 * rather than from the top down.
 */

set             : SET VARIABLE "=" STRING EOL { set($2, $4); }
                ;

hup             : HUP STRING
                  {
                      struct process *process;
                    
                      process = xmalloc(sizeof(struct process));
                      process->command = NULL;
                      process->pidfile = cfgexpand($2, "");
                      $$ = process;
                  }
                ;

/*
 * The arguments to a run action (=> run) are weird, because we need to
 * allocate the data structure up front and then resize it as we go if we run
 * out of space.  We don't want to use a more intuitive rule like:
 *
 *      arguments : STRING arguments
 *                |
 *
 * because the parser would then read the arguments in *reverse* (since the
 * null rule is the first thing to resolve), and each new argument we saw
 * would be the new argv[0] and we'd have to shift everything up in memory
 * each time.  So instead we turn this around, effectively pulling items off
 * the "end" of the current block being processed into we run out of them, at
 * which point the null alternative triggers, constructs the root of the list,
 * and then the stack of pending elements unwinds in the *forward* direction,
 * letting us read the arguments in order.  This may potentially break with a
 * *really* long command line, as this approach eats more parser stack memory;
 * for our purposes, this is not a worry.
 *
 * This is one of those constructs that I have to spend fifteen minutes
 * thinking about to remember quite how it works each time I see it again....
 * (This is probably proof I don't write enough grammars.)
 */

arguments       : arguments STRING
                  {
                      struct command *command = $1;
                      int size;

                      /*
                       * Check to see if we need to grow our array.  Note that
                       * the size has to be *larger* than argc so that we have
                       * room for the terminating NULL.
                       */
                      command->argc++;
                      if (command->argc >= command->size) {
                          command->size *= 2;
                          size = command->size * sizeof(char *);
                          command->argv = xrealloc(command->argv, size);
                      }

                      /*
                       * Aesthetically, the "name" argument of expand() should
                       * probably be the block label, but we don't have easy
                       * access to that here and my sense of aesthetics isn't
                       * strong enough to get me to restructure code to get at
                       * it....
                       */
                      command->argv[command->argc - 1] = cfgexpand($2, "");
                      command->argv[command->argc] = NULL;
                  }
                | /* empty */
                  {
                      struct command *command;
                    
                      command = xmalloc(sizeof(struct command));
                      command->argc = 0;
                      command->size = 3;
                      command->argv = xmalloc(3 * sizeof (char *));
                      command->next = NULL;
                      $$ = command;
                  }
                ;
                              
run             : RUN arguments
                {
                    struct process *process;
                  
                    process = xmalloc(sizeof(struct process));
                    process->command = $2;
                    process->pidfile = NULL;
                    $$ = process;
                }
                ;

process         : hup { $$ = $1; }
                | run { $$ = $1; }
                ;

stop            : STOP process EOL { $$ = $2; }
                | /* empty */ { $$ = NULL; }
                ;

restart         : RESTART process EOL { $$ = $2; }
                | /* empty */ { $$ = NULL; }
                ;

uid             : NUMBER { $$ = $1; }
                | STRING
                  {
                      struct passwd *pw;

                      pw = getpwnam($1);
                      if (pw != NULL) {
                          free($1);
                          $$ = pw->pw_uid;
                      } else {
                          cfgdie("Unknown user %s", $1);
                      }
                  }
                ;

gid             : NUMBER { $$ = $1; }
                | STRING
                  {
                      struct group *gr;

                      gr = getgrnam($1);
                      if (gr != NULL) {
                          free($1);
                          $$ = gr->gr_gid;
                      } else {
                          cfgdie("Unknown group %s", $1);
                      }
                  }
                ;

/*
 * log isn't necessary syntactically; it's here to pull out shared code
 * between the logs, archive, and copy syntax elements.
 */

log             : STRING
                  {
                      struct log *log;

                      log = xmalloc(sizeof(struct log));
                      log->name = $1;
                      log->rotate = false;
                      log->truncate = false;
                      log->analyze = NULL;
                      log->archive = NULL;
                      log->filter = NULL;
                      $$ = log;
                  }
                ;

logs            : LOG log STRING uid gid NUMBER EOL logs
                  {
                      struct log *log;
                      int mode, factor;

                      log = $2;
                      log->path = cfgexpand(lookup($3), log->name);
                      log->tmppath = NULL;
                      log->owner = (uid_t) $4;
                      log->group = (gid_t) $5;
                      mode = $6;

                      /*
                       * Interpret the permissions as octal.  Roll our own
                       * converter to avoid needless weird library calls.
                       */
                      factor = 1;
                      log->mode = 0;
                      while (mode > 0) {
                          log->mode += (mode % 10) * factor;
                          factor *= 8;
                          mode /= 10;
                      }

                      /* Return a linked list of what we've seen so far. */
                      log->next = $8;
                      $$ = log;
                  }
                | /* empty */ { $$ = NULL; }
                ;

analyze         : ANALYZE log arguments EOL
                  {
                      struct log *log;

                      log = $2;
                      log->analyze = $3;
                      $$ = log;
                  }
                ;

filter          : FILTER log arguments EOL
                  {
                      struct log *log;

                      log = $2;
                      log->filter = $3;
                      $$ = log;
                  }
                ;

archive         : ARCHIVE log STRING NUMBER EOL
                  {
                      struct log *log;

                      log = $2;
                      log->archive = archive($3, $4);
                      log->rotate = true;
                      $$ = log;
                  }
                ;

copy            : COPY log STRING EOL
                  {
                      struct log *log;

                      log = $2;
                      log->archive = archive($3, -1);
                      $$ = log;
                  }
                ;

truncate        : TRUNCATE log EOL
                  {
                      struct log *log;

                      log = $2;
                      log->truncate = true;
                      $$ = log;
                  }
                ;

action          : analyze { $$ = $1; }
                | archive { $$ = $1; }
                | filter { $$ = $1; }
                | copy { $$ = $1; }
                | truncate { $$ = $1; }
                ;

actions         : action actions
                  {
                      struct log *log;

                      log = $1;
                      log->next = $2;
                      $$ = log;
                  }
                | /* empty */ { $$ = NULL; }
                ;

block           : LABEL "{" stop restart logs actions "}"
                  {
                      struct class *block;

                      block = xmalloc(sizeof(struct class));
                      block->name = $1;
                      block->stop = $3;
                      block->restart = $4;
                      block->logs = merge($5, $6);
                      $$ = block;
                  }
                ;

/*
 * This is kind of ugly, since technically a variable set isn't a class at
 * all, but I couldn't think of a better name for this syntax element.
 */

class           : set class { $$ = $2; }
                | block class
                  {
                      struct class *block;

                      block = $1;
                      block->next = $2;
                      $$ = block;
                  }
                | /* empty */ { $$ = NULL; }
                ;

file            : class { class = $1; }
                ;

%%

/*
 * yyerror(), called by yacc code, is just a simple wrapper around cfgdie().
 */
void
yyerror(const char *error)
{
    cfgdie(error);
}


/*
 * Prepend the current line number to the error message before dying.  This
 * needs to not be a static function so that the lexer can pull it in and use
 * it as well.
 */
void
cfgdie(const char *format, ...)
{
    va_list message;

    fprintf(stderr, "config line %d: ", cfgline);
    va_start(message, format);
    vfprintf(stderr, format, message);
    va_end(message);
    fprintf(stderr, "\n");
    exit(1);
}

   
/*
 * We maintain a linked list of variables that have been set, which is used
 * later by lookup() when it does variable expansion.  Note that we don't
 * check for duplicates; this is intentional, as we want to allow people to
 * redefine variables.  We add all new definitions onto the *beginning* of the
 * linked list, and then any subsequent lookups will always find the latest
 * definition of a variable.
 */
static void
set(char *name, const char *content)
{
    struct variable *variable;

    variable = xmalloc(sizeof(struct variable));
    variable->name = name;
    variable->content = content;
    variable->next = variables ? variables : NULL;
    variables = variable;
}


/*
 * Given an argument that's supposed to be a path, run it through variable
 * expansion if it doesn't start with a slash.  Stuff starting with a slash
 * bypasses this entirely; it's somewhat counterintuitive that we still call
 * lookup for things that we're not going to look up in the variable table,
 * but it made the code neater.  We're being passed in a malloc'd string; if
 * we return a variable value (also a malloc'd string, stored in the linked
 * list of variables), be sure to free our argument.
 */
static char *
lookup(char *path)
{
    struct variable *variable;
    char *value;
  
    if (path[0] != '/' && strncmp(path, "./", 2) != 0) {
        for (variable = variables; variable != NULL; variable = variable->next)
            if (strcmp(variable->name, path) == 0)
                break;
        if (variable == NULL)
            cfgdie("Undefined variable %s", path);
        free(path);
        value = xstrdup(variable->content);
        return value;
    } else {
        return path;
    }
}


/*
 * Check over a string for the invalid %s escape and then pass it along to
 * expand() so that % expansion can be done.  The time passed to it will be 0
 * (indicating to use the current time).  Returns NULL on error.  We free the
 * string passed in and return the malloc'd string that expand() returns.
 */
static char *
cfgexpand(char *string, const char *name)
{
    char *p;

    /* Check for invalid % escapes. */
    for (p = string; *p; p++)
        if (*p == '%' && !(*(++p) && strchr("%dmnDMY", *p)))
            cfgdie("Invalid %% escape");

    /* Okay, valid format.  Send it through the real expand(). */
    p = expand(string, name, 0);
    if (p == NULL)
        cfgdie("Unable to expand string");
    free(string);
    return p;
}


/*
 * Construct an archive struct, given a template or variable containing a
 * template and the number of old logs to save as arguments.  We allocate a
 * new struct archive and return it.
 */
static struct archive *
archive(char *template, int count)
{
    struct archive *new;

    /* Allocate a new archive struct and initialize it. */
    new = xmalloc(sizeof(struct archive));
    new->count = count;
    new->path = NULL;
    new->next = NULL;

    /* Now massage the template according to rotation policy. */
    if (template == NULL) {
        new->template = NULL;
        new->ext = NULL;
    } else {
        new->template = lookup(template);
        new->ext = use_bzip2 ? "bz2" : "gz";
    }

    /* Return the completed archive struct. */
    return new;
}


/*
 * When we originally generated the list of logs and the list of actions, we
 * did so as two separate linked lists of logs, as it was easier to convince
 * yacc/bison to cope with that.  See the long comments at the top of the
 * grammer.  This merges them back together.
 *
 * We take two pointers to linked lists of logs; the first will be correct for
 * path, owner, group, and mode, and the second will have archive, rotate,
 * analyze, and filter elements filled in.  We return a single pointer to a
 * linked list of logs with no duplicates that has everything merged together.
 *
 * We have not previously checked to see if we have any duplicate log
 * definitions, so we also check this while we're at it.  We call cfgdie() on
 * any errors (rather than yyerror(), which can't take variable arguments).
 */
static struct log *
merge(struct log *logs, struct log *actions)
{
    struct log *current, *log, *old;
    struct archive *last;
    struct command *command;
  
    /* Scan logs and make sure that there aren't any duplicates. */
    for (current = logs; current != NULL && current->next != NULL;
         current = current->next)
        for (log = current->next; log != NULL; log = log->next)
            if (strcmp(current->name, log->name) == 0)
                cfgdie("Duplicate log definition for %s", current->name);

    /*
     * Now, step through the actions one at a time, merging their contents
     * into logs and freeing the memory used by the actions.  Here's where we
     * detect people assigning actions to logs that were never existed, so
     * those errors will show up as being at the end of the log class block
     * rather than at the archive line itself.  Hurm.
     */
    for (current = actions; current != NULL; ) {
        for (log = logs; log != NULL; log = log->next)
            if (strcmp(log->name, current->name) == 0)
                break;
        if (log == NULL)
            cfgdie("Unknown log %s", current->name);
        if (log->archive == NULL)
            log->archive = current->archive;
        else {
            for (last = log->archive; last->next != NULL; last = last->next)
                ;
            last->next = current->archive;
        }
        if (log->analyze == NULL) {
            log->analyze = current->analyze;
        } else {
            for (command = log->analyze; command->next != NULL;
                 command = command->next)
                ;
            command->next = current->analyze;
        }
        if (log->filter != NULL)
            cfgdie("Duplicate filter actions for %s", current->name);
        else
            log->filter = current->filter;
        log->rotate = current->rotate || log->rotate;
        log->truncate = current->truncate || log->truncate;
        if (log->truncate && log->rotate)
            cfgdie("Cannot both archive and truncate %s", current->name);
        old = current;
        current = current->next;
        free(old->name);
        free(old);
    }

    /* Return the linked list of combined log structs. */
    return logs;
}


/*
 * This is the function that the rest of the world gets to call.  It takes
 * in the name of the configuration file, and returns a pointer to a linked
 * list of class structs.
 */
struct class *
read_config(const char *filename)
{
    FILE *config;

    /* Attempt to open our configuration file. */
    config = fopen(filename, "r");
    if (config == NULL)
        die("Cannot open configuration file %s", filename);

    /* Point our lexer at the right file, and then let magic happen. */
    yyin = config;
    if (yyparse())
        die("Parsing of %s failed", filename);
    fclose(config);
    return class;
}
