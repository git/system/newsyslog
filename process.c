/*
 * Functions for running and signaling processes.
 *
 * Some general utility functions for running and signalling processes.  We
 * don't want to use system to run processes due to problems with the shell,
 * metacharacters, memory usage, and the like, so we have our own function
 * which just does a fork and exec.  There's also a function to signal a
 * program that writes a PID file with a SIGHUP to rebind to new logs (syslog
 * wants this, for example).
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1997, 1999, 2000, 2002, 2011
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/system.h>

#include <fcntl.h>
#include <signal.h>
#include <sys/wait.h>

#include <internal.h>
#include <util/messages.h>


/*
 * Execute a program with fork() and exec() and pass it an arbitrary number of
 * arguments.  This is a very limited reimplimentation of something like
 * execv() combined with system() which is designed to suit just our purposes.
 * Note that the argv array *has* to be NULL-terminated by the caller or evil
 * things happen.  Takes an optional second argument which is a path to a file
 * to pipe in to our child process.
 */
int
run(char *const argv[], const char *input)
{
    pid_t child;
    int status, fd;
    int success = 1;

    child = fork();
    if (child == (pid_t) -1) {
        syswarn("fork failed");
        success = 0;
    } else if (child == 0) {
        /* Supply our child's input if given. */
        if (input != NULL) {
            fd = open(input, O_RDONLY);
            if (fd < 0) {
                syswarn("open of %s failed", input);
                _exit(1);
            } else if (dup2(fd, 0) < 0) {
                syswarn("dup of %s to stdin failed", input);
                _exit(1);
            }
        }

        /* In child process, so exec the program. */
        execv(argv[0], argv);
        sysdie("exec failed");
    } else {
        /* In parent, wait for child to terminate. */
        child = wait(&status);
        if (child < 0) {
            syswarn("wait failed");
            success = 0;
        }
        if (!WIFEXITED(status)) {
            warn("%s terminated abnormally", argv[0]);
            success = 0;
        }
    }
    return success;
}


/*
 * Send a SIGHUP to a process, reading the PID of the process to SIGHUP from a
 * file.  This should be general enough to deal with syslogd and most
 * everything else that works similar to it.
 */
int
hup(const char *pid_filename)
{
    FILE *pid_file;
    int pid = 0;

    /* Read the PID from the given filename. */
    pid_file = fopen(pid_filename, "r");
    if (pid_file == NULL) {
        warn("Cannot open PID file %s", pid_filename);
        return 0;
    }
    fscanf(pid_file, "%d", &pid);
    fclose(pid_file);

    /* Careful not to kill process groups and *never* SIGHUP init. */
    if (pid < 2) {
        warn("Invalid PID %d in file %s", pid, pid_filename);
        return 0;
    }
    if (kill((pid_t) pid, SIGHUP) != 0) {
        syswarn("HUP %d from PID file %s failed", pid, pid_filename);
        return 0;
    }

    /* Sleep a couple of seconds to give the daemon time to reset. */
    sleep(2);

    /* Make sure the process is still running. */
    if (kill(pid, 0) != 0) {
        syswarn("Unable to verify process %d is running", pid);
        warn("Daemon HUPed from file %s may have died", pid_filename);
        return 0;
    }
    return 1;
}


/*
 * Open a pipe from a process.  Executes the program with fork() and exec()
 * and passes it an arbitrary number of arguments.  Takes an optional second
 * argument, which is a path to a file to pipe in to the process.  Returns a
 * FILE * to the open output from the process on success and NULL on failure.
 */
FILE *
open_pipe(char *const argv[], const char *input)
{
    pid_t child;
    int fds[2], fd;
    FILE *output;

    if (pipe(fds) != 0) {
        syswarn("pipe failed");
        return NULL;
    }
    child = fork();
    if (child == (pid_t) -1) {
        syswarn("fork failed");
        return NULL;
    } else if (child == 0) {
        close(fds[0]);
        if (dup2(fds[1], 1) < 0) {
            syswarn("dup of a pipe to stdout failed");
            _exit(1);
        }

        /* Supply our child's input if given. */
        if (input != NULL) {
            fd = open(input, O_RDONLY);
            if (fd < 0) {
                syswarn("open of %s failed", input);
                _exit(1);
            } else if (dup2(fd, 0) < 0) {
                syswarn("dup of %s to stdin failed", input);
                _exit(1);
            }
        }

        /* Exec the program. */
        execv(argv[0], argv);
        sysdie("exec failed");
    } else {
        close(fds[1]);
        output = fdopen(fds[0], "rb");
        if (output == NULL)
            syswarn("fdopen of a pipe failed");
        return output;
    }
}


/*
 * Close a pipe and wait for the child to terminate.  Returns true if the
 * child exited normally and false otherwise.
 */
int
close_pipe(FILE *output)
{
    int status;
    pid_t child;

    fclose(output);
    child = wait(&status);
    if (child < 0) {
        syswarn("wait failed");
        return 0;
    }
    if (!WIFEXITED(status)) {
        warn("filter process terminated abnormally");
        return 0;
    }
    return 1;
}
