#!/usr/bin/perl
# $Id$
# Test suite for newsyslog errors.

$| = 1;
print "1..2\n";

# Run newsyslog on the specified configuration file, returning its exit status
# and its error output as a list.
sub run_newsyslog {
    my $config = shift;
    my $pid = open (NEWSYSLOG, '-|');
    my $output;
    if ($pid == 0) {
        open (STDERR, '>&STDOUT') or die "Can't dup stdout: $!\n";
        exec ('../newsyslog', $config) or die "Unable to exec: $!\n";
    } elsif ($pid < 0) {
        die "Unable to fork: $!\n";
    } else {
        local $/;
        $output = <NEWSYSLOG>;
        close NEWSYSLOG;
        waitpid ($pid, 0) or die "waitpid failed: $!\n";
    }
    return ($?, $output);
}

# Set up the directory structure that newsyslog will use.
if (!-f 'basic.t') {
    if (-f 't/basic.t')       { chdir 't'    or die "Can't cd to t: $!\n" }
    elsif (-f '../basic.t')   { chdir '..'   or die "Can't cd to ..: $!\n" }
    elsif (-f '../t/basic.t') { chdir '../t' or die "Can't cd to ../t: $!\n" }
    else { die "Can't find root of test directory.\n" }
}
mkdir ('logs', 0755);

# Try running newsyslog on the directory.
my ($status, $output) = run_newsyslog ('./error-dir.conf');
print 'not ' if ($status == 0);
print "ok 1\n";
my $wanted = "./logs is not a regular file\nSkipping archiving of test\n";
if ($output ne $wanted) {
    print "# SAW: $output";
    print "# NOT: $wanted";
    print 'not ';
}
print "ok 2\n";

# Clean up.
system ('rm', '-rf', 'logs');
