/*
 * Functions to manipulate (copy, move, etc.) files.
 *
 * A set of functions to rename, copy, and create files, including doing some
 * specialized things we want to do because of the sorts of files that we're
 * moving.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1997, 1998, 1999, 2000, 2002, 2011
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/system.h>

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <zlib.h>

#include <internal.h>
#include <util/messages.h>
#include <util/xmalloc.h>


/*
 * Rename an existing log file so that we can rebind the daemon to a new log
 * file.  Logs are always renamed to <log_name>.<time> where <time> is the
 * current time in seconds since the epoch, possibly modified by skew_seconds.
 * This should reduce our chances of namespace collision to virtually nil.  We
 * return a pointer to the new name of the log on success and NULL on failure;
 * memory for the new name is malloc()ed and the caller is responsible for
 * freeing it.
 */
char *
rename_unique(const char *filename)
{
    char *newname;
    time_t now;

    /* Allow 20 characters for time to make sure the sun's gone. */
    now = time(NULL) - (skew_minutes * 60);
    xasprintf(&newname, "%s.%lu", filename, (unsigned long) now);
    if (rename(filename, newname) != 0) {
        syswarn("Rename of %s failed", filename);
        free(newname);
        return NULL;
    }
    return newname;
}


/*
 * Do the magic of keeping a number of old logs, numbered consecutively (such
 * as log.0.gz, log.1.gz, log.2.gz, etc.).  Delete the highest numbered log
 * and then rename all logs numbered lower than that to make room for a new .0
 * log.
 */
bool
shuffle_files(const char *filename, const char *extension, int count)
{
    char *logname;
    char *newlogname = NULL;
    int i;

    /* Remove the highest numbered log, if it exists. */
    xasprintf(&logname, "%s.%d.%s", filename, count - 1, extension);
    if (access(logname, F_OK) == 0) {
        if (unlink(logname) != 0) {
            syswarn("Unable to delete %s", logname);
            goto fail;
        }
    }

    /* Rename each log to the next highest number. */
    for (i = count - 2; i >= 0; i--) {
        free(logname);
        if (newlogname != NULL)
            free(newlogname);
        xasprintf(&logname, "%s.%d.%s", filename, i, extension);
        xasprintf(&newlogname, "%s.%d.%s", filename, i + 1, extension);
        if (access(logname, F_OK) == 0) {
            if (rename(logname, newlogname) != 0) {
                syswarn("Unable to rename %s", logname);
                goto fail;
            }
        }
    }

    /* All done.  Clean up the memory we allocated. */
    free(logname);
    free(newlogname);
    return true;

 fail:
    free(logname);
    if (newlogname != NULL)
        free(newlogname);
    return false;
}


/*
 * Copy a file from one name file to another.  We do this ourselves rather
 * than forking a cp process so that we can be sure of the error checking (and
 * to avoid external program dependencies).
 */
bool
copy(const char *sourcename, const char *destname)
{
    FILE *source, *dest;
    struct stat statbuf;
    int nread, nwritten;
    bool have_size;
    off_t size = 0;
    mode_t mode = 0;

    /* Buffer for transferring the file. */
    static char buffer[2048];

    /* Open the two files. */
    source = fopen(sourcename, "rb");
    if (source == NULL) {
        syswarn("Unable to open source file %s", sourcename);
        return false;
    }
    dest = fopen(destname, "wb");
    if (dest == NULL) {
        warn("Cannot create %s", destname);
        return false;
    }

    /* Get the original size of the file in bytes. */
    if (fstat(fileno(source), &statbuf) != 0) {
        syswarn("Unable to stat source file %s", sourcename);
        have_size = false;
    } else {
        size = statbuf.st_size;
        mode = statbuf.st_mode;
        have_size = true;
    }

    /* If we can, chmod the archived log to the same as the original. */
    if (have_size && fchmod(fileno(dest), mode) != 0)
        syswarn("Unable to chmod destination file %s", destname);

    /* Copy the file over. */
    while (!feof(source)) {
        nread = fread(buffer, 1, 2047, source);
        nwritten = fwrite(buffer, 1, nread, dest);
        if (nread != nwritten || ferror(source)) {
            warn("Error while writing %s", destname);
            fclose(source);
            fclose(dest);
            return false;
        }
    }

    /* Close the files and then verify that the copy was successful. */
    fclose(source);
    if (fclose(dest) != 0) {
        warn("Error while writing %s", destname);
        return false;
    }
    if (!have_size || stat(destname, &statbuf) != 0)
        warn("Unable to verify successful copy of %s", sourcename);
    else if (size != statbuf.st_size) {
        warn("Error copying %s", sourcename);
        warn("  size of copy (%lu) != size of original (%lu)",
             (unsigned long) statbuf.st_size, (unsigned long) size);
        return false;
    }

    return true;
}


/*
 * Create a new file with the given name, owner, group, and permissions.
 */
bool
create_file(const char *file, uid_t owner, gid_t group, mode_t mode)
{
    int fd;

    fd = open(file, O_CREAT | O_WRONLY, mode);
    if (fd < 0) {
        syswarn("Creation of %s failed", file);
        return false;
    }
    if (fchown(fd, owner, group) != 0)
        syswarn("Unable to chown %s", file);
    if (fchmod(fd, mode) != 0)
        syswarn("Unable to chmod %s", file);
    close(fd);
    return true;
}


/*
 * Truncate an existing file and ensure the owner, group, and permissions.
 */
bool
truncate_file(const char *file, uid_t owner, gid_t group, mode_t mode)
{
    int fd;

    fd = open(file, O_CREAT | O_TRUNC | O_WRONLY, mode);
    if (fd < 0) {
        syswarn("Truncation of %s failed", file);
        return false;
    }
    if (fchown(fd, owner, group) != 0)
        syswarn("Unable to chown %s", file);
    if (fchmod(fd, mode) != 0)
        syswarn("Unable to chmod %s", file);
    close(fd);
    return true;
}


/*
 * Creates the parent directory of a file, if it doesn't already exist.
 */
void
create_directory(char *path)
{
    char *separator;

    separator = strrchr(path, '/');
    if (separator != NULL) {
        *separator = '\0';
        if (access(path, W_OK) != 0)
            if (mkdir(path, 0755) != 0 && errno != EEXIST)
                syswarn("Could not create directory %s", path);
        *separator = '/';
    }
}
