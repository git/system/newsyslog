/*
 * Functions to handle log file compression.
 *
 * The set of functions that handle the compression of logs and streams.
 * There are two interfaces exposed here, compress_file and compress_stream,
 * which take in data and write it out in a compressed format.  The supported
 * compression methods are gzip and bzip2, and which to use is determined by
 * the extension on the destination path passed to the interface functions.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1997, 1998, 1999, 2000, 2002, 2011
 *     The Board of Trustees of the Leland Stanford Junior University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>
#include <portable/system.h>

#if HAVE_LIBBZ2
# include <bzlib.h>
#endif
#include <fcntl.h>
#include <sys/stat.h>
#if HAVE_LIBZ
# include <zlib.h>
#endif

#include <internal.h>
#include <util/messages.h>


/*
 * Given an open FILE * as the source of log data to save, write it into the
 * given location, compressing with zlib.  Returns true on success, false on
 * failure.
 */
#if !HAVE_LIBZ

static bool
gzip_stream(FILE *source UNUSED, int fd UNUSED)
{
    warn("gzip compression support not available");
    return false;
}

#else

static bool
gzip_stream(FILE *source, int fd)
{
    gzFile *dest;
    int nread, nwritten;
    static char buffer[2048];

    /* Now, open this file descriptor as a maximally compressed file. */
    dest = gzdopen(fd, "wb9");
    if (dest == NULL) {
        warn("Cannot begin compressing");
        close(fd);
        return false;
    }

    /* Copy the file over.  Maybe some day add better error reporting. */
    while (!feof(source) && !ferror(source)) {
        nread = fread(buffer, 1, 2047, source);
        nwritten = gzwrite(dest, buffer, nread);
        if (nread != nwritten || ferror(source)) {
            warn("Error while writing compressed stream");
            gzclose(dest);
            return false;
        }
    }

    /*
     * Close the destination file and then verify that the copy was
     * successful.
     */
    if (gzclose(dest) != 0) {
        warn("Error while writing compressed stream");
        return false;
    }
    return true;
}

#endif /* HAVE_LIBZ */


/*
 * Given an open FILE * as the source of log data to save, write it into the
 * given location, compressing with bzip2.  Returns true on success, false on
 * failure.
 */
#if !HAVE_LIBBZ2

static bool
bzip2_stream(FILE *source UNUSED, int fd UNUSED)
{
    warn("bzip2 compression support not available");
    return false;
}

#else

static bool
bzip2_stream(FILE *source, int fd)
{
    BZFILE *dest;
    int nread, nwritten;
    static char buffer[2048];

    /* Now, open this file descriptor as a maximally compressed file. */
    dest = BZ2_bzdopen(fd, "wb");
    if (dest == NULL) {
        warn("Cannot begin compressing");
        close(fd);
        return false;
    }

    /* Copy the file over.  Maybe some day add better error reporting. */
    while (!feof(source) && !ferror(source)) {
        nread = fread(buffer, 1, 2047, source);
        nwritten = BZ2_bzwrite(dest, buffer, nread);
        if (nread != nwritten || ferror(source)) {
            warn("Error while writing compressed stream");
            BZ2_bzclose(dest);
            return false;
        }
    }

    /* Close the destination file. */
    BZ2_bzclose(dest);
    return true;
}

#endif /* HAVE_LIBBZ2 */


/*
 * Given an open FILE * as the source of log data to save, write it into the
 * given location, compressing as appropriate for the extension of the
 * destination file.  If mode is non-zero, fchmod the destination file to that
 * mode.  Returns true on success, false on failure.
 */
bool
compress_stream(FILE *source, const char *destname, mode_t mode)
{
    int fd;
    size_t destlen;

    /* Open the destination file and try to chmod it to the right perms. */
    fd = open(destname, O_CREAT | O_EXCL | O_WRONLY, 0600);
    if (fd < 0) {
        syswarn("Cannot create %s", destname);
        return false;
    }
    if (mode != 0 && fchmod(fd, mode) != 0)
        syswarn("Unable to chmod destination file %s", destname);

    /* Dispatch to the appropriate function based on the destination name. */
    destlen = strlen(destname);
    if (destlen > 4 && !strcmp(destname + destlen - 4, ".bz2"))
        return bzip2_stream(source, fd);
    else if (destlen > 3 && !strcmp(destname + destlen - 3, ".gz"))
        return gzip_stream(source, fd);
    else {
        warn("Cannot determine compression type for %s", destname);
        return false;
    }
}


/*
 * Copy a file from one location to another, compressing it in the process.
 */
bool
compress_file(const char *sourcename, const char *destname)
{
    FILE *source;
    struct stat statbuf;
    bool status;
    mode_t mode;

    /* Open the source file as a standard file and get its mode. */
    source = fopen(sourcename, "rb");
    if (source == NULL) {
        syswarn("Unable to open source file %s", sourcename);
        return false;
    }
    if (fstat(fileno(source), &statbuf) == 0)
        mode = statbuf.st_mode;
    else {
        syswarn("Unable to stat source file %s", sourcename);
        mode = 0;
    }

    /* Do the work and close the source file. */
    status = compress_stream(source, destname, mode);
    fclose(source);
    return status;
}
